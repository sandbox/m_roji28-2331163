<?php

/**
 * @file
 * Admin page.
 */

/**
 * Return form array.
 */
function menu_edit_items_attributes_configuration_form($form, &$form_state) {
  $form['menu_edit_items_attributes_method'] = array(
    '#title' => 'Method to build attributes for &lt;ul&gt;.',
    '#type' => 'radios',
    '#default_value' => variable_get('menu_edit_items_attributes_method', 'alt'),
    '#options' => array(
      'alt' => t('Alter theme_menu_tree() function'),
      'js' => t('Use javascript'),
    ),
    'alt' => array(
      '#description' => t("Recommended but don't use this option if your theme has implemented theme_menu_tree in template.php (e.g bartik_menu_tree)."),
    ),
    'js' => array(
      '#description' => t("Build empty link with javascript, it dosn't work if client's javascript disabled."),
    ),
  );
  $form['actions']['#type'] = 'actions';
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save configuration'),
  );
  return $form;
}

/**
 * Handle submit.
 */
function menu_edit_items_attributes_configuration_form_submit($form, &$form_state) {
  // If changed, we need rebuild theme registry.
  if ($form['menu_edit_items_attributes_method']['#default_value'] != $form['menu_edit_items_attributes_method']['#value']) {
    // Clear.
    drupal_theme_rebuild();
    // Build again.
    drupal_theme_initialize();
    variable_set('menu_edit_items_attributes_method', $form_state['values']['menu_edit_items_attributes_method']);
    // Save.
    drupal_set_message(t('Theme registry rebuild.'));
  }
}
