/**
 * @file
 * Javascript for display pages.
 */

(function($) {

Drupal.behaviors.menuEditItemsAttributes = {
  attach: function (context, settings) {
    $('.menu-edit-items-attributes', context).once('menu-edit-items-attributes', function () {
      console.log(this);
      $li = $(this);
      var id = $li.attr('menu-edit-items-attributes');
      $ul = $li.parent();
      var _setting = settings.menu_edit_items_attributes[id];
      new Drupal.menuEditItemsAttributesDisplay($ul, _setting);
    });
  }
};

Drupal.menuEditItemsAttributesDisplay = function ($ul, _setting) {
  if (typeof _setting.attributes != 'undefined') {
    for(attribute in _setting.attributes){
      var value = _setting.attributes[attribute];
      if (typeof value == 'object'){
        var value = value.join(' ');
      }
      var oldvalue = $ul.attr(attribute);
      if (typeof oldvalue !== 'undefined') {
        value = oldvalue + ' ' + value;
      }
      $ul.attr(attribute, value);
    }
  }
  if (typeof _setting.settings.remove_ul_class_menu != 'undefined') {
    var new_class = new Array();
    var _class = $ul.attr('class').split(' ');
    var found = $.inArray('menu', _class);
    if (found > -1) {
      _class.splice(found,1);
      var value = _class.join(' ');
      $ul.attr('class', value);
    }
  }
}

})(jQuery);
