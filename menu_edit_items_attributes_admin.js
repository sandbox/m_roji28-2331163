/**
 * @file
 * Javascript for admin pages.
 */

(function($) {

Drupal.behaviors.menuEditItemsAttributes = {
  attach: function (context, settings) {
    $('#toggle-display-column input', context).once('toggle-display-column', function () {
      new Drupal.menuEditItemsAttributes(this);
    });
  }
};

Drupal.menuEditItemsAttributes = function (selector) {
  var master = this;
  var target = $(selector).attr('data-target');
  this.input = selector;
  this.$target = $('.' + target);
  master.onclick();
  $(selector).click(function(){
    master.onclick();
  });
}
Drupal.menuEditItemsAttributes.prototype.onclick = function () {
  // console.log(this);
  var input = this.input
  var $target = this.$target
  if (input.checked) {
    $target.show();
  }
  else{
    $target.hide();
  }
}
})(jQuery);
