<?php

/**
 * @file
 * Module file.
 */

/**
 * Implements hook_menu().
 */
function menu_edit_items_menu() {
  $path = $_path = 'admin/structure/menu/manage/%menu/menu_edit_items';
  $items = array();
  $items[$path] = array(
    'title' => 'Edit Items',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('menu_edit_items_overview_form', 4),
    'access arguments' => array('administer menu'),
    'type' => MENU_LOCAL_TASK,
    'weight' => 10,
    'file' => 'menu_edit_items.admin.inc',
  );
  // Build default secondary tab named Standard.
  $path = $_path . '/default';
  $items[$path] = array(
    'title' => 'Default',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -1,
  );
  // Add another secondary tabs.
  $info = module_invoke_all('menu_edit_items_info');
  foreach ($info as $key => $values) {
    $path = $_path . '/' . $key;
    if (!isset($items[$path])) {
      // Build title.
      if (!isset($info[$key]['title'])) {
        $title = 'More';
      }
      elseif (is_string($info[$key]['title'])) {
        $title = $info[$key]['title'];
      }
      elseif (is_array($info[$key]['title']) && isset($info[$key]['title'][0])) {
        $title = $info[$key]['title'][0];
      }
      // Register path.
      $items[$path] = array(
        'title' => $title,
        'page callback' => 'drupal_get_form',
        'page arguments' => array('menu_edit_items_overview_form', 4, 6),
        'access arguments' => array('administer menu'),
        'type' => MENU_LOCAL_TASK,
        'weight' => isset($info[$key]['weight']) ? $info[$key]['weight'] : 20,
        'file' => 'menu_edit_items.admin.inc',
      );
    }
  }
  return $items;
};

/**
 * Implements hook_theme().
 */
function menu_edit_items_theme($existing, $type, $theme, $path) {
  return array(
    'menu_edit_items_overview_form' => array(
      'file' => 'menu_edit_items.admin.inc',
      'render element' => 'form',
    ),
  );
};

/**
 * Implements hook_menu_edit_items_info().
 *
 * Add default column to standard.
 */
function menu_edit_items_menu_edit_items_info() {
  return array(
    'default' => array(
      'callback' => 'menu_edit_items_form',
    ),
  );
}

/**
 * Add default column to Tab Standard, as same as menu_edit_item().
 */
function menu_edit_items_form(&$form, &$form_state) {
  // dpm($form,'$form');
  foreach (element_children($form) as $mlid) {
    if (isset($form[$mlid]['#item'])) {
      $item = $form[$mlid]['#item'];
      // dpm($item, '$item');
      $form[$mlid]['hidden'] = array(
        '#type' => 'checkbox',
        '#title' => t('Enable @title menu link', array('@title' => $item['title'])),
        '#title_display' => 'invisible',
        '#default_value' => !$item['hidden'],
        '#column' => 'Enabled',
      );
      $form[$mlid]['expanded'] = array(
        '#type' => 'checkbox',
        '#title' => t('Expanded @title menu link', array('@title' => $item['title'])),
        '#title_display' => 'invisible',
        '#default_value' => $item['expanded'],
        '#column' => 'Expanded',
      );
      $form[$mlid]['link_title'] = array(
        '#type' => 'textfield',
        '#title' => t('Set title menu link'),
        '#title_display' => 'invisible',
        '#default_value' => $item['link_title'],
        '#column' => 'Title',
        '#required' => TRUE,
      );
      if ($item['module'] == 'menu') {
        $form[$mlid]['link_path'] = array(
          '#type' => 'textfield',
          '#title' => t('Set path menu link'),
          '#title_display' => 'invisible',
          '#default_value' => $item['link_path'],
          '#column' => 'Path',
          '#required' => TRUE,
        );
      }
      else {
        $form[$mlid]['link_path'] = array(
          '#type' => 'textfield',
          '#disabled' => TRUE,
          '#title' => t('Path'),
          '#title_display' => 'invisible',
          '#value' => $item['link_path'],
          '#column' => 'Path',
        );
      }
    }
  }
}

/**
 * Implements hook_menu_edit_items_validate_alter().
 */
function menu_edit_items_menu_edit_items_validate_alter(&$form, &$form_state) {
  foreach ($form_state['values'] as $mlid => $values) {
    if (is_array($form_state['values'][$mlid]) && isset($form_state['values'][$mlid]['link_path'])) {
      _menu_edit_items_menu_edit_items_validate($mlid, 'link_path', $form, $form_state);
    }
  }
}

/**
 * Validate each items.
 *
 * This function modify from menu_edit_item_validate().
 */
function _menu_edit_items_menu_edit_items_validate($mlid, $column, &$form, &$form_state) {
  $item = &$form_state['values'][$mlid];
  $normal_path = drupal_get_normal_path($item['link_path']);
  if ($item['link_path'] != $normal_path) {
    drupal_set_message(t('The menu system stores system paths only, but will use the URL alias for display. %link_path has been stored as %normal_path', array('%link_path' => $item['link_path'], '%normal_path' => $normal_path)));
    $item['link_path'] = $normal_path;
  }
  if (!url_is_external($item['link_path'])) {
    $parsed_link = parse_url($item['link_path']);
    if (isset($parsed_link['query'])) {
      $form_state['values'][$mlid]['options']['query'] = drupal_get_query_array($parsed_link['query']);
    }
    else {
      // Use unset() rather than setting to empty string
      // to avoid redundant serialized data being stored.
      unset($form_state['values'][$mlid]['options']['query']);
    }
    if (isset($parsed_link['fragment'])) {
      $form_state['values'][$mlid]['options']['fragment'] = $parsed_link['fragment'];
    }
    else {
      unset($form_state['values'][$mlid]['options']['fragment']);
    }
    if (isset($parsed_link['path']) && $item['link_path'] != $parsed_link['path']) {
      $item['link_path'] = $parsed_link['path'];
    }
  }
  if (!trim($item['link_path']) || !drupal_valid_path($item['link_path'], TRUE)) {
    form_set_error($mlid . '][link_path', t("The path '@link_path' is either invalid or you do not have access to it.", array('@link_path' => $item['link_path'])));
  }
}

/**
 * Implements hook_menu_edit_items_submit_alter().
 *
 * Submit handler.
 */
function menu_edit_items_menu_edit_items_submit_alter($form, $form_state, &$updated_items) {
  foreach (element_children($form) as $mlid) {
    if (isset($form[$mlid]['#item'])) {
      foreach (element_children($form[$mlid]) as $column) {
        _menu_edit_items_menu_edit_items_submit($mlid, $column, $form, $form_state, $updated_items);
      }
    }
  }
}

/**
 * Update each items.
 */
function _menu_edit_items_menu_edit_items_submit($mlid, $column, $form, $form_state, &$updated_items) {
  // Only different.
  $column_element = $form[$mlid][$column];
  if (!isset($column_element['#value']) || !isset($column_element['#default_value']) || $column_element['#value'] == $column_element['#default_value']) {
    return;
  }
  // Load first if not exists.
  if (!isset($updated_items[$mlid])) {
    $updated_items[$mlid] = $form[$mlid]['#item'];
  }
  // Updated.
  switch ($column) {
    case 'link_title':
      $updated_items[$mlid]['link_title'] = $form_state['values'][$mlid][$column];
      break;

    case 'link_path':
      $updated_items[$mlid]['link_path'] = $form_state['values'][$mlid][$column];
      // Link path, is possibility provide a new column named options.
      if (isset($form_state['values'][$mlid]['options'])) {
        $old_options = isset($updated_items[$mlid]['options']) ? $updated_items[$mlid]['options'] : array();
        $updated_items[$mlid]['options'] = array_merge($old_options, $form_state['values'][$mlid]['options']);
      }
      break;

    case 'expanded':
      $updated_items[$mlid]['expanded'] = $form_state['values'][$mlid][$column];
      break;

    // Hidden is a special case, the value needs to be reversed.
    case 'hidden':
      // Convert to integer rather than boolean due to PDO cast to string.
      $updated_items[$mlid]['hidden'] = $form_state['values'][$mlid][$column] ? 0 : 1;
      break;
  }
}
