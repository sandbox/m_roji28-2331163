MENU EDIT ITEMS
===============

CONTENTS OF THIS FILE
---------------------
 * Overview
 * Installation
 * Configuration
 * Recommended Module


Overview
--------
 * Are you tired to editing menu items one by one? This module helps you edit
   all menu items in one page. You can edit all fields such as:
   title, path, expanded, and enabled.
 * No conflict with module [Menu attributes].
 * Sub module "Menu Edit Items Attributes" provide:
   * three page (new secondary tab) to edit all attribute for element html
     <ul>, <li>, and </a>.
   * ability to remove default class="leaf/first/last" in li element that
     given by Drupal.
   * ability to remove default class="menu" in ul element that
     given by Drupal.
 * Sub module "Menu Edit Items Attributes Class" provide:
   * one page (new secondary tab) to edit attribute class for element html
     <ul>, <li>, and </a>.
 * Sub module "Menu Edit Items Icon" provide:
   * one page (new secondary tab) to edit all icon for menu items
     (integrated with module [Icon API][icon]).

   [Menu attributes]:https://www.drupal.org/project/menu_attributes
   [icon]:https://www.drupal.org/project/icon


Installation
------------
Install as you would normally install a contributed drupal module. See:
https://drupal.org/documentation/install/modules-themes/modules-7
for further information.

Configuration
-------------
No need configuration, just visit
**admin/structure/menu/manage/(MENU)/menu_edit_items**
which (MENU) is your menu machine_name then you can edit all items
in one menu.


Recommended Module
------------------
List modules which integrates itself with us.

1. [Empty Link][2]

  [1]:https://www.drupal.org/sandbox/m_roji28/2332541
  [2]:https://www.drupal.org/sandbox/m_roji28/2296979
