<?php
/**
 * @file
 * Provide API to using by other modules.
 */

/**
 * Register your path.
 *
 * Add function to alter the master form which provide by
 * menu_edit_items_overview_form() and define to path you want to exists.
 *
 * Your function to alter form set in callback key. You don't need add
 * function in form[#submit]. Use hook_menu_edit_items_submit_alter()
 * instead to handle submit.
 *
 * Your callback function name is recommend to use this pattern:
 * MODULE_menu_edit_items_form
 * eg. menu_edit_items_classes_menu_edit_items_form.
 */
function hook_menu_edit_items_info() {
  $info = array(
    // Build a secondary tab below Menu Edit Items and named Standard.
    // Use mymodule_custom_element() to alter menu_edit_items_overview_form().
    'standard' => array(
      'title' => 'Standard',
      'callback' => 'mymodule_custom_element',
    ),

    // Build a secondary tab below Menu Edit Items and if you don't give it a
    // name in title, then the name will be provided automatically.
    // Use mymodule_custom_element_2() to alter
    // menu_edit_items_overview_form().
    'css_classes' => array(
      'callback' => 'mymodule_custom_element_2',
    ),

    // If there are others module that use exists key (eg: css_classes),
    // which give different callback, so the callback will be merged.
    // This is because Drupal's hook use array_merge_recursive().
    'css_classes' => array(
      'callback' => 'mymodule_custom_element_3',
    ),

    // For exampe above, both of mymodule_custom_element_2() and
    // mymodule_custom_element_3() will be run.
  );
  return $info;
}

/**
 * Alter submit.
 */
function hook_menu_edit_items_submit_alter($form, $form_state, &$updated_items) {

}

/**
 * Alter validate.
 */
function hook_menu_edit_items_validate_alter(&$form, &$form_state) {

}
