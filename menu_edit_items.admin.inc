<?php

/**
 * @file
 * Admin page.
 */

/**
 * Return form array.
 *
 * This function is a master. It's a default form to give element #weight and
 * provide draggable to table. If you want alter this form, don't use
 * hook_form_alter(), use hook_menu_edit_items_info() instead.
 *
 * Use hook_menu_edit_items_info(),  in addition to giving you an form_alter
 * also give you definition of link_path.
 */
function menu_edit_items_overview_form($form, &$form_state, $menu, $secondary_tab = NULL) {
  // Get default form from menu overview.
  module_load_include('inc', 'menu', 'menu.admin');
  $form = menu_overview_form($form, $form_state, $menu);

  // Submit in the below.
  $form['actions']['#weight'] = 100;
  $form['actions']['submit']['#weight'] = 100;

  // We have to remove colon ":", in $mlid, because can make #state not work.
  // In file states.js line 109 (Drupal 7.31), when variable selector
  // contains colon,
  // it makes error in console:
  // uncaught exception: Syntax error, unrecognized expression: 576.
  foreach (element_children($form) as $mlid) {
    if (preg_match('/^mlid\:\d+/', $mlid)) {
      $new_mlid = str_replace(':', '', $mlid);
      $form[$new_mlid] = $form[$mlid];
      unset($form[$mlid]);
    }
  }

  foreach (element_children($form) as $mlid) {
    if (isset($form[$mlid]['#item'])) {
      unset($form[$mlid]['operations']);
      // Hidden handled by form alter in this module.
      unset($form[$mlid]['hidden']);
      $element = &$form[$mlid];
      if (isset($element['plid'])) {
        $element['plid']['#attributes']['class'][] = 'menu-plid';
        $element['plid']['#column'] = 'Weight';
        $element['plid']['#weight'] = 1;
      }
      if (isset($element['hidden'])) {
        $element['hidden']['#column'] = array('data' => t('Enabled'), 'class' => array('checkbox'));
        $element['hidden']['#cell_attributes'] = array('class' => array('checkbox', 'menu-enabled'));
        $element['weight']['#weight'] = -1;
      }
      if (isset($element['mlid'])) {
        $element['mlid']['#attributes']['class'][] = 'menu-mlid';
        $element['mlid']['#column'] = 'Weight';
        $element['mlid']['#weight'] = 2;
      }
      if (isset($element['weight'])) {
        $element['weight']['#attributes']['class'][] = 'menu-weight';
        $element['weight']['#column'] = 'Weight';
        $element['weight']['#weight'] = 0;
      }
    }
  }
  $info = module_invoke_all('menu_edit_items_info');
  if (is_null($secondary_tab)) {
    $secondary_tab = 'default';
  }
  $info = module_invoke_all('menu_edit_items_info');
  if (isset($info[$secondary_tab]['callback'])) {
    $callback = $info[$secondary_tab]['callback'];
    is_array($callback) or $callback = (array) $callback;
    foreach ($callback as $f) {
      if (function_exists($f)) {
        $f($form, $form_state);
      }
    }
  }
  $form['#attached']['css'][] = drupal_get_path('module', 'menu_edit_items') . '/menu_edit_items.css';
  return $form;
}

/**
 * Handle default validate.
 */
function menu_edit_items_overview_form_validate($form, &$form_state) {
  // Alter from others module.
  drupal_alter('menu_edit_items_validate', $form, $form_state);
}

/**
 * Handle default submit.
 */
function menu_edit_items_overview_form_submit($form, &$form_state) {
  // Code below is default to handle weight and plid
  // source from function menu_overview_form_submit()
  // in module menu.
  // Every element instead of weight and plid should be handle
  // by hook_menu_edit_items_submit().
  $order = array_flip(array_keys($form_state['input']));
  $form = array_merge($order, $form);
  $updated_items = array();
  $fields = array('weight', 'plid');
  foreach (element_children($form) as $mlid) {
    if (isset($form[$mlid]['#item'])) {
      $element = $form[$mlid];
      // Update any fields that have changed in this menu item.
      foreach ($fields as $field) {
        if ($element[$field]['#value'] != $element[$field]['#default_value']) {
          $element['#item'][$field] = $element[$field]['#value'];
          $updated_items[$mlid] = $element['#item'];
        }
      }
    }
  }

  // Alter from others module.
  drupal_alter('menu_edit_items_submit', $form, $form_state, $updated_items);

  // Save all our changed items to the database.
  foreach ($updated_items as $item) {
    $item['customized'] = 1;
    menu_link_save($item);
  }
  if ($count = count($updated_items)) {
    $output = format_plural($count, '1 item has been saved.', '@count items has been saved.');
    drupal_set_message($output);
  }
}

/**
 * Theming.
 *
 * This theme is more flexible than theme_menu_overview_form().
 * Other modules can build own column in table and
 * ability to give attributes in <td>,
 */
function theme_menu_edit_items_overview_form($variables) {
  $form = $variables['form'];

  drupal_add_tabledrag('menu-edit-items', 'match', 'parent', 'menu-plid', 'menu-plid', 'menu-mlid', TRUE, MENU_MAX_DEPTH - 1);
  drupal_add_tabledrag('menu-edit-items', 'order', 'sibling', 'menu-weight');

  $header = array(
    t('Menu link'),
  );
  $rows = array();
  foreach (element_children($form) as $mlid) {
    if (isset($form[$mlid]['#item'])) {
      $form[$mlid]['#printed'] = TRUE;
      $element = &$form[$mlid];

      $cols = array();

      // Add first column to title.
      $cols[] = theme('indentation', array('size' => $element['#item']['depth'] - 1)) . drupal_render($element['title']);
      // Other column, is building by #column.
      foreach (element_children($element, TRUE) as $cell) {

        // Skip element has printed (title) and don't have #column information.
        if ((isset($element[$cell]['#printed']) && $element[$cell]['#printed']) || !isset($element[$cell]['#column'])) {
          continue;
        }
        $column = $element[$cell]['#column'];
        if (is_string($column)) {
          $column = array('data' => $column);
        }
        $column_title = $column['data'];

        // Add info column to header.
        if (!isset($header[$column_title])) {
          $header[$column_title] = $column;
        }

        // Render cell to column.
        if (!isset($cols[$column_title])) {
          $cols[$column_title] = '';
        }
        $_cell = array('data' => drupal_render($element[$cell]));
        if (!isset($cols[$column_title]['data'])) {
          $cols[$column_title]['data'] = '';
        }
        $cols[$column_title]['data'] .= $_cell['data'];
        if (isset($element[$cell]['#cell_attributes'])) {
          $cols[$column_title] = array_merge($cols[$column_title], $element[$cell]['#cell_attributes']);
        }
      }
      $cols = array_merge(array('data' => $cols), $element['#attributes']);
      $cols['class'][] = 'draggable';
      $rows[] = $cols;
    }
  }

  // Render html.
  $output = '';
  if (empty($rows)) {
    $rows[] = array(array('data' => $form['#empty_text'], 'colspan' => '7'));
  }
  $table = theme('table', array(
    'header' => $header,
    'rows' => $rows,
    'attributes' => array('id' => 'menu-edit-items'),
    )
  );
  $form['table'] = array(
    '#type' => 'markup',
    '#markup' => $table,
    '#weight' => 0,
  );
  // Make sort to false.
  $form['#sorted'] = FALSE;
  $output .= drupal_render_children($form);
  return $output;
}
